$(function(){

	function getData(){
		$.getJSON('https://api.github.com/repos/angular/angular.js/commits',function( data ) {
			var html = '';
			$.each(data, function(i, v) {
				var color = v.sha.match(/\d$/)? 'light-blue':'';
				var message = v.commit.message.length > 40 ? v.commit.message.substr(0, 40) + ' ...' : v.commit.message;

				html += '<div class="row '+ color +'">\
					<div class="pull-left avatar-container">\
						<img class="avatar" src="' + v.author.avatar_url+ '">\
					</div>\
					<div class="pull-left description">\
						<b>'+ message +'</b>\
						<div>'+ v.commit.author.name +'</div>\
					</div>\
					<div class="sha">\
						commit:'+ v.sha +'\
					</div>\
				</div>';
			});
			$('.row-container').html(html);
		});
	}

	getData();
	
	$(document).off('click', '.refresh').on('click', '.refresh', function() {
		getData();
	});
});
